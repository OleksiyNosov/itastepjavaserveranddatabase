import java.sql.ResultSet;
import java.sql.SQLException;

public class Genre implements IMySqlTable {
    private int id;
    private String name;

    public Genre() {}

    public Genre(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTableName() {
        return "genres";
    }

    public IMySqlTable createInstance(ResultSet resultSet) throws SQLException {
        return new Genre(
            resultSet.getInt(1),
            resultSet.getString(2)
        );
    }

    public void fillAssociations() {

    }
}
