import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class Movie implements IMySqlTable {
    private int id;
    private String title;
    private Date releaseDate;

    private ArrayList<Actor> actors;
    private ArrayList<Genre> genres;

    public Movie() {}

    public Movie(int id, String title, Date releaseDate, ArrayList<Actor> actors, ArrayList<Genre> genres) {
        this.id = id;
        this.title = title;
        this.releaseDate = releaseDate;
        this.actors = actors;
        this.genres = genres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public ArrayList<Actor> getActors() {
        return actors;
    }

    public void setActors(ArrayList<Actor> actors) {
        this.actors = actors;
    }

    public ArrayList<Genre> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<Genre> genres) {
        this.genres = genres;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Title: \"");
        stringBuilder.append(title);
        stringBuilder.append("\" ");

        stringBuilder.append("Release Date: ");
        stringBuilder.append(releaseDate.toString());
        stringBuilder.append(" ");

        return stringBuilder.toString();
    }

    public String getTableName() {
        return "movies";
    }

    public IMySqlTable createInstance(ResultSet resultSet) throws SQLException {
        return new Movie(
            resultSet.getInt(1),
            resultSet.getString(2),
            resultSet.getDate(3),
            null,
            null
        );
    }

    public void fillAssociations() {
        setActors((ArrayList<Actor>)(ArrayList<?>) MySqlDatabaseConnector.select(new Actor()));
        setGenres((ArrayList<Genre>)(ArrayList<?>) MySqlDatabaseConnector.select(new Genre()));
    }
}
