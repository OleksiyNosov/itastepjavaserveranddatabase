import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        server.createContext("/movies", new MyHandler());
        server.setExecutor(null);
        server.start();
    }

    static class MyHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            String response = createResponse();

            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }

        private String createResponse() {
            ArrayList<Movie> movies = (ArrayList<Movie>)(ArrayList<?>) MySqlDatabaseConnector.select(new Movie());

            StringBuilder response = new StringBuilder();

            response.append("<!DOCTYPE html>");
            response.append("<html>");

            for (Movie movie : movies) {
                response.append(movie.toString());
                System.out.println(movie.toString());
                response.append("<br>");
            }

            response.append("</html>");

            return response.toString();
        }
    }
}
