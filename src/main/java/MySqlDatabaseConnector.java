import java.sql.*;
import java.util.ArrayList;

public class MySqlDatabaseConnector {
    private static final String url = "jdbc:mysql://localhost:3306/the_pirate_bar";
    private static final String user = "root";
    private static final String password = "";

    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;

    public static ArrayList<IMySqlTable> select(IMySqlTable iMySqlTable) {
        String query = "SELECT * FROM " + iMySqlTable.getTableName() + ";";

        ArrayList<IMySqlTable> items = new ArrayList<IMySqlTable>();

        try {
            connection = DriverManager.getConnection(url, user, password);
            if(!connection.isClosed()) {
                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                while (resultSet.next()) {
                    items.add(iMySqlTable.createInstance(resultSet));
                }

                for (IMySqlTable item: items) {
                    item.fillAssociations();
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {}
            try {
                statement.close();
            } catch (SQLException e) {}
        }

        return items;
    }
}
