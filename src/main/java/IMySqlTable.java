import java.sql.ResultSet;
import java.sql.SQLException;

public interface IMySqlTable {
    String getTableName();
    IMySqlTable createInstance(ResultSet resultSet) throws SQLException;
    void fillAssociations();
}
